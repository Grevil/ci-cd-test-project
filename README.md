# DROWL Base Theme

## Development

1. run npm install
2. run gulp build
3. run release-it to publish a new release (to Gitlab & Packagist)

TODO: more details to come ...

## Default Breakpoints

- SM: 0 - 640 (incl, image width 640 Px)
- MD: 641 - 1024 (incl, image width 1024 Px)
- LG: 1025 - 1366 (incl, image width 1366 Px)
- XL: 1367 - 1920 (incl, image width 1920 Px)
- XXL: 1921 - ∞ (image width 2560 Px)

## Iconfont "drowl-base-ico"

The DROWL Base theme brings it own iconfont to ensure the icons are reliably available.

They use the classes prefix .ico-db-.

The iconset is managed with icomoon.io.
