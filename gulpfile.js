// See: https://www.drupal.org/docs/8/theming/creating-automation-tools-for-custom-themes-gulpjs

// This gulpfile is used for gitlab-ci, you can also use it in your subtheme.

// =============================================================================
// ====================== Project Configuration ================================
// =============================================================================

// Check out gulpfile.variables.js

const { watch, series, parallel, src, dest, lastRun } = require('gulp');

// Plugins
const plumber = require('gulp-plumber'),
  rename = require('gulp-rename'),
  ifElse = require('gulp-if-else'),
  babel = require('gulp-babel'),
  uglify = require('gulp-uglify'),
  gulp_remove_logging = require('gulp-remove-logging'),
  sftp = require('gulp-sftp'),
  // livereload = require('gulp-livereload'),
  sassInheritance = require('gulp-sass-inheritance'),
  sourcemaps = require('gulp-sourcemaps'),
  sass = require('gulp-sass')(require('sass')),
  postcss = require('gulp-postcss'),
  cleanCss = require('gulp-clean-css'),
  autoprefixer = require('autoprefixer'),
  modernizr = require('gulp-modernizr-build'),
  argument = require('yargs').argv,
  run = require('gulp-run-command').default,
  notify = require('gulp-notify');

var config = require('./gulpfile.variables.js');

// Console colors (see: https://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color)
const successColor = '\x1b[32m',
  infoColor = '\x1b[36m',
  warningColor = '\x1b[33m',
  alertColor = '\x1b[33m';

// Activate devmode with parameter
// Usage: gulp --devmode
if (argument.devmode) {
  config.devmode = true;
}

function returnDevmodeStatus() {
  if (config.devmode === true) {
    console.log(warningColor, 'Devmode status: ' + config.devmode);
  } else {
    console.log(infoColor, 'Devmode status: ' + config.devmode + ' (add flag --devmode to activate)');
  }
}

// =============================================================================
// =========================== Helper Functions ================================
// =============================================================================

copy = function (src, dest) {
  return src(src).pipe(dest(dest));
};

// NPM Update
function npmUpdate(cb) {
  run('npm update')();
  cb();
}

// =============================================================================
// ============== SFTP connection data for direct upload =======================
// =============================================================================

sftpConnectionData = {
  host: config.host,
  user: config.user,
  agent: config.agent,
  agentForward: config.agentForward,
  port: config.port,
  remotePath: config.remotePath,
};

// =============================================================================
// =========================== Define Tasks ====================================
// =============================================================================

// Move npm libraries to frontend_libraries folder
var libsToMove = ['./node_modules/drowl-base-theme-iconset/**'];
function moveFrontendLibraries() {
  return src(libsToMove, {
    // gulp needs some src. but the src of the modernizr build plugin is a static path in the plugin (node_modules....)
    base: './node_modules/',
    since: lastRun(moveFrontendLibraries),
  })
    .pipe(plumber())
    .pipe(dest('frontend_libraries'));
}

// Scripts Tasks
function buildScripts() {
  return src(config.jsRegex, {
    since: lastRun(buildScripts),
  })
    .pipe(plumber()) // Hint: Plumber always need to be piped first!
    .pipe(
      rename({
        suffix: '.min',
      }),
    )
    .pipe(
      ifElse(
        config.devmode === false,
        // Minitfy / Uglify
        function () {
          return gulp_remove_logging();
        },
      ),
    )
    .pipe(
      ifElse(
        config.devmode === false,
        // Minitfy / Uglify
        function () {
          return babel({
            presets: ['@babel/env'],
          });
        },
      ),
    )
    .pipe(
      ifElse(
        config.devmode === false,
        // Minitfy / Uglify
        function () {
          return uglify();
        },
      ),
    )
    .pipe(
      notify({
        message: config.devmode ? 'Scripts: finished!' : 'Scripts: minify' + ' finished!',
        onLast: true,
      }),
    )
    .pipe(dest(config.folderJs));
}
function uploadScripts() {
  // Add target path to sft connection data
  var scriptsSftpConnectionData = Object.assign({}, sftpConnectionData);
  scriptsSftpConnectionData.remotePath += '/' + config.folderJs;

  return (
    src(config.jsDistRegex, {
      since: lastRun(uploadScripts),
    })
      // TODO: is plumber still a thing? This was a workaround to a node.js bug, seems fixed in meantime
      .pipe(plumber()) // Hint: Plumber always need to be piped first!
      // Upload changed files
      .pipe(sftp(scriptsSftpConnectionData))
      .pipe(
        notify({
          message: config.devmode ? 'Scripts: upload finished!' : 'Scripts: minify + upload finished!',
          onLast: true,
        }),
      )
  );
}

// SASS Compiling plus uploading changed files (detection of changed files just work while watching!)
function buildSass() {
  return (
    src(config.sassRegex, {
      since: lastRun(buildSass),
    })
      //find files that depend on the files that have changed
      .pipe(
        sassInheritance({
          dir: 'scss/',
        }),
      )
      .pipe(plumber()) // Hint: Plumber always need to be piped first!
      .pipe(sourcemaps.init())
      .pipe(sass.sync().on('error', sass.logError))
      .pipe(
        postcss([autoprefixer(), require('postcss-nested')], {
          syntax: require('postcss-scss'),
        }),
      )
      .pipe(
        rename({
          suffix: '.min',
        }),
      )
      .pipe(
        ifElse(
          config.devmode === false,
          // Production: Minitfy / Uglify
          cleanCss,
          // Dev: Create Sourcemap
          function () {
            return sourcemaps.write('.');
          },
        ),
      )
      .pipe(
        notify({
          message: config.devmode ? 'SASS: finished!' : 'CSS: minify finished!',
          onLast: true,
        }),
      )
      .pipe(dest('./' + config.folderCss))
  );
}
function uploadCss() {
  // Add target path to sft connection data
  var cssSftpConnectionData = Object.assign({}, sftpConnectionData);
  cssSftpConnectionData.remotePath += '/' + config.folderCss;

  return (
    src(config.cssDistRegex, {
      since: lastRun(uploadCss),
    })
      // TODO: is plumber still a thing? This was a workaround to a node.js bug, seems fixed in meantime
      .pipe(plumber()) // Hint: Plumber always need to be piped first!
      // Upload changed files
      .pipe(sftp(cssSftpConnectionData))
      .pipe(
        notify({
          message: config.devmode ? 'CSS: upload finished!' : 'CSS: minify + upload finished!',
          onLast: true,
        }),
      )
  );
}

// Template Files
function templatesUpload() {
  // Add target path to sft connection data
  var templatesSftpConnectionData = Object.assign({}, sftpConnectionData);
  templatesSftpConnectionData.remotePath += '/' + config.folderTemplates;

  return (
    src(config.tplRegex, {
      since: lastRun(templatesUpload),
    })
      // Upload changed files
      .pipe(sftp(templatesSftpConnectionData))
      .pipe(
        notify({
          message: 'Templates: upload finished!',
          onLast: true,
        }),
      )
  );
}

// Images Upload
function imagesUpload() {
  // Add target path to sft connection data
  var imagesSftpConnectionData = Object.assign({}, sftpConnectionData);
  imagesSftpConnectionData.remotePath += '/';

  return (
    src(config.imagesRegex, {
      since: lastRun(imagesUpload),
    })
      // Upload changed files
      .pipe(sftp(imagesSftpConnectionData))
      .pipe(
        notify({
          message: 'Images: upload finished!',
          onLast: true,
        }),
      )
  );
}

// Fonts Upload
function fontsUpload() {
  // Add target path to sft connection data
  var fontsSftpConnectionData = Object.assign({}, sftpConnectionData);
  fontsSftpConnectionData.remotePath += '/' + config.folderFonts;

  return (
    src(config.fontsRegex, {
      since: lastRun(fontsUpload),
    })
      // Upload changed files
      .pipe(sftp(fontsSftpConnectionData))
      .pipe(
        notify({
          message: 'Fonts: upload finished!',
          onLast: true,
        }),
      )
  );
}

// Configuration Files (YML & Co)
function configUpload() {
  // Add target path to sft connection data
  var configSftpConnectionData = Object.assign({}, sftpConnectionData);
  configSftpConnectionData.remotePath += '/';

  return (
    src(config.configRegex, {
      since: lastRun(configUpload),
    })
      // Upload changed files
      .pipe(sftp(configSftpConnectionData))
      .pipe(
        notify({
          message: 'Config: upload finished!',
          onLast: true,
        }),
      )
  );
}

// Build custom modernizr based on feature detects inside the src JS files (and manually added tests)
function buildModernizr() {
  // Add target path to sft connection data
  var bmSftpConnectionData = Object.assign({}, sftpConnectionData);
  bmSftpConnectionData.remotePath += '/frontend_libraries/';

  return src(config.jsRegex, {
    // gulp needs some src. but the src of the modernizr build plugin is a static path in the plugin (node_modules....)
    since: lastRun(buildModernizr),
  })
    .pipe(plumber())
    .pipe(
      modernizr('modernizr.js', {
        cssPrefix: 'mdnzr-',
        addFeatures: ['css/cssgrid', 'css/flexbox'], // Adding some tests manually
      }),
    )
    .pipe(
      rename({
        suffix: '.custom.min',
      }),
    )
    .pipe(uglify())
    .pipe(
      notify({
        message: 'Move Frontend Libraries: finished!',
        onLast: true,
      }),
    )
    .pipe(dest('frontend_libraries'));
}

function uploadFrontendLibraries() {
  // Add target path to sft connection data
  var flSftpConnectionData = Object.assign({}, sftpConnectionData);
  flSftpConnectionData.remotePath += '/' + config.folderFrontendLibraries;

  return (
    src(config.frontendLibrariesRegex, {
      since: lastRun(uploadFrontendLibraries),
    })
      .pipe(plumber()) // Hint: Plumber always need to be piped first!
      // Upload changed files
      .pipe(sftp(flSftpConnectionData))
      .pipe(
        notify({
          message: 'Frontend Libraries: upload finished!',
          onLast: true,
        }),
      )
  );
}

// TODO: Browserreload doesnt work well within WSL enviroments - so we skip this thing for now.
function browserreload() {
  // Enable if needed. Livereload sucks, cause it only supports one instance at the same time and throws an error if there already is a running livereload session.
  return src(config.sassRegex).pipe(plumber()); // Hint: Plumber always need to be piped first!
  // .pipe(livereload({
  //   quiet: true
  // }))
}

// =======================================================
// Public tasks (usable with gulp TASK)
// =======================================================

exports.watch = function () {
  returnDevmodeStatus();
  watch(config.jsRegex, series(buildScripts));
  watch(config.sassRegex, series(buildSass));
};
exports.watchUpload = function () {
  returnDevmodeStatus();
  watch(config.jsRegex, series(buildScripts, uploadScripts, browserreload));
  watch(config.sassRegex, series(buildSass, uploadCss, browserreload));
  watch(config.tplRegex, series(templatesUpload, browserreload));
  watch(config.configRegex, series(configUpload));
  watch(config.imagesRegex, series(imagesUpload));
  watch(config.fontsRegex, series(fontsUpload));
  // Enable if needed. Livereload in WSL sucks, cause it only supports one instance at the same time and throws an error if there already is a running livereload session.
  // livereload.listen();
};

// TODO: Automaticaly run this task once after npm install & in gitlab CI if a new release is created.
exports.build = function (done) {
  returnDevmodeStatus();
  var doBuild = series(buildModernizr, moveFrontendLibraries, buildScripts, buildSass);
  doBuild();
  done();
};

// Just upload the current state
exports.pushOnline = function (done) {
  returnDevmodeStatus();
  var doPushOnline = series(
    uploadFrontendLibraries,
    uploadScripts,
    uploadCss,
    templatesUpload,
    configUpload,
    imagesUpload,
    fontsUpload,
  );
  doPushOnline();
  done();
};

exports.default = series(watch);
