<?php
/**
* @file
* Add custom theme settings to the ZURB Foundation sub-theme.
* !! Remember to add default values to config/install/drowl_base.settings.yml
*/

use Drupal\Core\Form\FormStateInterface;

/**
* Implements hook_form_FORM_ID_alter().
* @param $form
* @param \Drupal\Core\Form\FormStateInterface $form_state
*/
function drowl_base_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  // Remove some Base theme settings we won't use
  // TODO: Removed this. If removed here, some settings wont work at all or
  //       we need to set theme_get_setting('setting', 'zurb_foundation') the
  //       second parameter to get it from the base theme.
  //       Maybe simply remove this shit and set the missing values hard right here.

  // =============== Define our theme settings / modify existing ones ==========

  // Region Settings
  $form['region_settings'] = [
    '#type' => 'details',
    '#title' => t('Region Settings'),
    '#weight' => -5,
    '#open' => FALSE,
  ];
  // Hide sidebars on frontpage
  $form['region_settings']['drowl_base_hide_sidebars_on_frontpage'] = [
    '#type' => 'checkbox',
    '#title' => t('Hide Sidebars on Frontpage'),
    '#default_value' => theme_get_setting('drowl_base_hide_sidebars_on_frontpage'),
  ];
  // UI Elements group (from base theme, just changed the order)
  $form['theme_ui']['#weight'] = -3;
  // Status messages in reveal (base theme, just override the label)
  $form['theme_ui']['zurb_foundation_status_in_reveal']['#title'] = t('<strong>Messages:</strong> Display status messages in Reveal');
  // Language switcher
  $form['theme_ui']['drowl_base_lang_switcher_use_country_shortcut'] = [
    '#type' => 'checkbox',
    '#title' => t('<strong>Language Switcher:</strong> Use country shortcut instead of fullname'),
    '#default_value' => theme_get_setting('drowl_base_lang_switcher_use_country_shortcut'),
  ];
  // Styles & Scripts
  $form['styles_scripts']['#weight'] = -2;
  // Form defaults
  $form['theme_forms'] = [
    '#type' => 'details',
    '#title' => t('Form defaults'),
    '#weight' => -1,
    '#open' => FALSE,
  ];
  $form['theme_forms']['drowl_base_form_submit_default_btn_style'] = [
    '#type' => 'textfield',
    '#title' => t('Default form submit button style'),
    '#description' => t('CSS Class-(color)-name for submit buttons, eg: "primary" or "primary hollow"'),
    '#default_value' => theme_get_setting('drowl_base_form_submit_default_btn_style'),
  ];
  // Page elements (core)
  $form['page_elements']['#weight'] = 0;
  $form['page_elements']['zurb_foundation_page_site_logo']['#title'] = t('Show site logo in page header');
  $form['page_elements']['drowl_base_topbar_logo_behavoir'] = [
    '#type' => 'select',
    '#title' => t('Top Bar Logo (small) behavoir'),
    '#description' => t('Select how the small logo should behave inside the top bar (you can upload the small logo beyond).'),
    '#options' => [
      'hide' => t('Hide'),
      'show_permanent' => t('Show permanent'),
      'show_scrolled' => t('Show if scrolled down'),
      'show_permanent_shrink_scrolled' => t('Show permanent, but shrink if scrolled down'),
    ],
    '#default_value' => theme_get_setting('drowl_base_topbar_logo_behavoir'),
  ];
  $form['page_elements']['drowl_base_show_header_site_logo'] = [
    '#type' => 'checkbox',
    '#title' => t('Show page logo in header'),
    '#default_value' => theme_get_setting('drowl_base_show_header_site_logo'),
  ];
  $form['page_elements']['drowl_base_show_header_site_name'] = [
    '#type' => 'checkbox',
    '#title' => t('Show page name in header'),
    '#default_value' => theme_get_setting('drowl_base_show_header_site_name'),
  ];
  $form['page_elements']['drowl_base_show_header_site_slogan'] = [
    '#type' => 'checkbox',
    '#title' => t('Show page slogan in header'),
    '#default_value' => theme_get_setting('drowl_base_show_header_site_slogan'),
  ];

  // Add upload field for a small version of the customer logo - usecase eg.: The docked menu bar
  $form['theme_logo_small'] = [
    '#type' => 'details',
    '#title' => t('Smaller Logo (a tiny variation of the logo)'),
    '#weight' => 8,
    '#open' => TRUE,
  ];
  $form['theme_logo_small']['drowl_base_page_site_logo_small_use_default'] = [
    '#type' => 'checkbox',
    '#title' => t('Use the logo supplied by the theme'),
    '#default_value' => theme_get_setting('drowl_base_page_site_logo_small_use_default'),
  ];
  $form['theme_logo_small']['drowl_base_page_site_logo_small_path'] = [
    '#type' => 'textfield',
    '#title' => t('Path to custom logo (small)'),
    '#description' => t('Examples: logo.svg (for a file in the public filesystem), public://logo.svg, or themes/custom/drowl_child/logo.svg.'),
    '#default_value' => theme_get_setting('drowl_base_page_site_logo_small_path'),
  ];
  $form['theme_logo_small']['drowl_base_page_site_logo_small'] = [
    '#title' => t('Logo (small) upload'),
    '#type' => 'managed_file',
    '#description' => t("If you don't have direct file access to the server, use this field to upload your logo."),
    '#default_value' => theme_get_setting('drowl_base_page_site_logo_small'),
    '#upload_location' => 'public://branding/',
  ];

  $form['logo']['theme_logo_alt'] = [
    '#type' => 'textfield',
    '#title' => t('Logo alt attribute'),
    '#description' => t('Sets the logo alt attribute for SEO. <strong>Leave empty (default) to use the site name as alt text.</strong>'),
    '#default_value' => theme_get_setting('theme_logo_alt'),
  ];
  $form['logo']['theme_logo_title'] = [
    '#type' => 'textfield',
    '#title' => t('Logo title attribute'),
    '#description' => t('Sets the logo title attribute for SEO. <strong>Leave empty (default) to use the site name as title text.</strong>'),
    '#default_value' => theme_get_setting('theme_logo_title'),
  ];
  // Furthermore - move logo and favicon to bottom also
  $form['logo']['#weight'] = 7;
  $form['favicon']['#weight'] = 9;
}
