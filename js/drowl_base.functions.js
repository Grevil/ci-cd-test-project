/**
 * @file
 * Themes custom functions collection.
 *
 */
(function ($, Drupal) {
  // Drupal.behaviors.drowl_base_functions = {
  //   attach: function (context, settings) {

  //   }
  // };

  // Workaround: Ensure Drupal.drowl_base exists because in some situations
  // this file is loaded before drowl_base/drowl_base - nobody (TF&JP) knows why...
  if (!Drupal.drowl_base) {
    Drupal.drowl_base = {};
  }
  // Create drowl_base sub-namespace:
  if (!Drupal.drowl_base.functions) {
    Drupal.drowl_base.functions = {};
  }

  /**
   * Select fake placeholders:
   * - Colorize the the current selected option to the default placeholder color
   *   while the first option is selected.
   *
   * Initialize by class / by adding the needed additonal wrapper, example:
   * $('.form-select--fake-placeholder', context).selectFakePlaceholder();
   */
  $.fn.selectFakePlaceholder = function () {
    $(this).each(function () {
      var $select = $(this);
      if ($select.children().first().is(':selected')) {
        $select.addClass('select-placeholder-option-active');
      } else {
        $select.removeClass('select-placeholder-option-active');
      }
      $select.on('change', function () {
        if ($(this).children().first().is(':selected')) {
          $(this).addClass('select-placeholder-option-active');
        } else {
          $(this).removeClass('select-placeholder-option-active');
        }
      });
    });
  };

  /**
   * Determine the "Page Header"-Height, what means everything above the
   * .header-image-wrapper.
   */
  Drupal.drowl_base.functions.getPageHeaderHeight = function () {
    var headerHeightSum = 0;
    var $metaHeader = $('.page-meta-header:first');
    var $pageHeader = $('.page-header:first');
    var $topBar = $('.top-bar-outer:first');

    if ($metaHeader.length) {
      var headerHeightSum = headerHeightSum + $metaHeader.outerHeight(true);
    }
    if ($pageHeader.length) {
      var headerHeightSum = headerHeightSum + $pageHeader.outerHeight(true);
    }
    if ($topBar.length) {
      var headerHeightSum = headerHeightSum + $topBar.outerHeight(true);
    }

    return headerHeightSum;
  };

  /**
   * Scroll to top functionality:
   */
  Drupal.drowl_base.functions.scrollToTop = function () {
    var element = $('body');
    var offset = element.offset();
    var offsetTop = offset.top;
    $('html, body').animate(
      {
        scrollTop: offsetTop,
      },
      500,
      'linear',
    );
  };
})(jQuery, Drupal);
