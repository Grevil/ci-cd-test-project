/**
 * @file
 * EU Cookie compliance module additions.
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_base_eucc = {
    attach: function (context, settings) {
      $(document).ready(function () {
        // Set the current banners height as css variable
        document.documentElement.style.setProperty('--eucc-banner-height', $('#sliding-popup').outerHeight() + 'px');
        document.documentElement.style.setProperty(
          '--eucc-withdraw-tab-height',
          $('#sliding-popup .eu-cookie-withdraw-tab:first').outerHeight() + 'px',
        );
        // Update height
        $('#sliding-popup').on('eu_cookie_compliance_popup_close eu_cookie_compliance_popup_open', function () {
          document.documentElement.style.setProperty('--eucc-banner-height', $('#sliding-popup').outerHeight() + 'px');
          document.documentElement.style.setProperty(
            '--eucc-withdraw-tab-height',
            $('#sliding-popup .eu-cookie-withdraw-tab:first').outerHeight() + 'px',
          );
        });
        // Add further EUCC banner wrapper to archive the style we want
        if ($('#sliding-popup').length) {
          if (!$('#sliding-popup').parents('.eucc-outer').length) {
            $('#sliding-popup', context).wrap('<div class="eucc-outer"></div>');
          }
        } else {
          console.log('EUCC #sliding-popup not found!');
        }
      });
    },
  };
})(jQuery, Drupal);
