/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_base_protip = {
    attach: function (context, settings) {
      // Initalize protip
      // http://protip.rocks/
      $.protip({
        defaults: {
          // scheme: 'pro',
          // arrow: true
        },
      });
    },
  };

  // Create drowl_base sub-namespace:
  if (!Drupal.drowl_base.protip) {
    Drupal.drowl_base.protip = {};
  }
})(jQuery, Drupal);
