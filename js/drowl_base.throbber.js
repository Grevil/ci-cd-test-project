/**
 * @file
 * Replaced Drupal cores ajax throbber(s), see: https://www.drupal.org/node/2974681
 *
 */
(function ($, Drupal) {
  Drupal.theme.ajaxProgressThrobber = function () {
    var throbberMarkup =
      "<div class='ajax-spinner ajax-spinner--inline'> \
        <div class='spinner spinner--small'> \
          <div class='sk-circle'> \
            <div class='sk-circle1 sk-child'></div> \
            <div class='sk-circle2 sk-child'></div> \
            <div class='sk-circle3 sk-child'></div> \
            <div class='sk-circle4 sk-child'></div> \
            <div class='sk-circle5 sk-child'></div> \
            <div class='sk-circle6 sk-child'></div> \
            <div class='sk-circle7 sk-child'></div> \
            <div class='sk-circle8 sk-child'></div> \
            <div class='sk-circle9 sk-child'></div> \
            <div class='sk-circle10 sk-child'></div> \
            <div class='sk-circle11 sk-child'></div> \
            <div class='sk-circle12 sk-child'></div> \
          </div> \
        </div> \
        <span class='ajax-spinner__label visually-hidden'> \
          " +
      Drupal.t('Loading&nbsp;&hellip;', {}, { context: 'Loading text for Drupal cores Ajax throbber (inline)' }) +
      ' \
        </span> \
      </div>';
    return throbberMarkup;
  };

  Drupal.theme.ajaxProgressIndicatorFullscreen = function () {
    var throbberMarkup =
      "<div class='ajax-spinner ajax-spinner--fullscreen'> \
        <div class='spinner'> \
          <div class='double-bounce1'></div> \
          <div class='double-bounce2'></div> \
        </div> \
        <span class='ajax-spinner__label'> \
            " +
      Drupal.t('Loading&nbsp;&hellip;', {}, { context: 'Loading text for Drupal cores Ajax throbber (fullscreen)' }) +
      ' \
        </span> \
      </div>';
    return throbberMarkup;
  };

  // You can also customize only throbber message:
  // Drupal.theme.ajaxProgressMessage = message => '<div class="my-message">' + message + '</div>';

  // Create drowl_base sub-namespace:
  if (!Drupal.drowl_base) {
    Drupal.drowl_base = {};
  }
  if (!Drupal.drowl_base.throbber) {
    Drupal.drowl_base.throbber = {};
  }
})(jQuery, Drupal);
