/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_base_expandable_inline_search = {
    attach: function (context, settings) {
      // Dynamic search (block) form
      $(function () {
        $('.block--expandable-inline-search-form input.form-search', context).each(function () {
          var $searchField = $(this);
          var $blockWrapper = $(this).parents('.block:first');
          var $blockWrapperInner = $blockWrapper.children('.block__content');
          var $topbar = $(this).parents('.top-bar:first');
          var originPlaceholder = Drupal.t('Search');
          var activePlaceholder = Drupal.t('Enter your searchphrase');
          // First of all add a simple span with the paceholder text. We need it to determine the origin width of the search field without ugly spacings
          // Alle the needed css styles go to [type="search"] .. so.. not really clean but works well.
          // We needed to solve it this way, bacause all browser engines renders the text differently.
          $searchField.before(
            '<span class="form-search form-search-width-indicator" type="search">' + originPlaceholder + '</span>',
          );
          $searchField.css('width', $('.form-search-width-indicator:first').outerWidth());
          $('.form-search-width-indicator:first').addClass('hidden');
          setTimeout(function () {
            $blockWrapper.addClass('initialized');
          }, 500);
          $searchField.on('focus', function () {
            // Reserve space - the expanded class makes the textfield pos. absolute
            $blockWrapper.css('width', $blockWrapper.outerWidth()).css('height', $blockWrapper.outerHeight());
            // Change placeholder text and add indicator class
            $blockWrapperInner.addClass('expanded').addClass('animated-search-processed');
            $topbar.addClass('top-bar--search-expanded');
            $(this).attr('placeholder', activePlaceholder);
          });
          $searchField.on('focusout', function () {
            $blockWrapperInner.removeClass('expanded');
            $(this).attr('placeholder', originPlaceholder);
            $topbar.removeClass('top-bar--search-expanded');
          });
        });
      });
    },
  };

  // Create drowl_base sub-namespace:
  if (!Drupal.drowl_base.parallax) {
    Drupal.drowl_base.parallax = {};
  }
})(jQuery, Drupal);
