/**
 * @file
 * Collapse views filters on smaller devices.
 * ==========================================
 * You need to add the markup by yourself, and set the "data-expanded-device-size" attribute with an existing foundation
 * size as value. On this breakpoint the filters become collapsed and the trigger button is hidden.
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_base_expandable_filters = {
    attach: function (context, settings) {
      $('.expandable-filters', context)
        .once('expandable-filters')
        .each(function () {
          var $wrapper = $(this);
          var $trigger_button = $wrapper.find('.expandable-filters__trigger-button:first');
          var expanded_device_size = $wrapper.data('expanded-device-size');
          if (Foundation.MediaQuery.atLeast(expanded_device_size)) {
            $wrapper.addClass('expanded');
          } else {
            $wrapper.addClass('expandable-filters--active');
          }
          // Register button no matter what size
          $trigger_button.on('click', function (event) {
            event.preventDefault(); // prevent the button from submitting the form
            $wrapper.toggleClass('expanded');
          });
        });
    },
  };

  // Create drowl_base sub-namespace:
  if (!Drupal.drowl_base.expandable_filters) {
    Drupal.drowl_base.expandable_filters = {};
  }
})(jQuery, Drupal);
