/**
 * @file
 * Overrides the webform jquery ui tooltip implementation with protip
 * ==========================================
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_base_webform_help = {
    attach: function (context, settings) {
      $('.webform-element-help', context).each(function () {
        var $webformHelpTrigger = $(this);
        $(this).protipSet({
          title: $webformHelpTrigger.attr('data-webform-help'),
          scheme: 'white',
          size: 'small',
        });
      });
    },
  };

  // Create drowl_base sub-namespace:
  if (!Drupal.drowl_base.drowl_base_webform_help) {
    Drupal.drowl_base.drowl_base_webform_help = {};
  }
})(jQuery, Drupal);
