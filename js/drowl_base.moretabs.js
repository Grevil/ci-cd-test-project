/**
 * @file
 * Hides overflowing tabs inside a "more"-dropdown menu
 * Implementation based on https://css-tricks.com/container-adapting-tabs-with-more-button (https://codepen.io/osvaldas/pen/xWNLXy)
 *
 * Consider using these polyfills to broaden browser support:
 * — https://www.npmjs.com/package/classlist-polyfill
 * — https://www.npmjs.com/package/nodelist-foreach-polyfill
 */

(function ($, Drupal) {
  Drupal.behaviors.drowl_base_moretabs = {
    attach: function (context, settings) {
      // Initialize by class / by adding the needed additonal wrapper
      $('.moretabs .tabs', context).moreTabs();
    },
  };

  // Create drowl_base sub-namespace:
  if (!Drupal.drowl_base.moretabs) {
    Drupal.drowl_base.moretabs = {};
  }

  $.fn.moreTabs = function (moretext) {
    // Initialize default variables
    // TODO: Add parameters (& default values)?
    // charamount = (charamount === undefined ? 180 : charamount);
    moretext =
      moretext === undefined ? Drupal.t('more', {}, { context: "'Show more' button for hidden tabs" }) : moretext;

    $(this).each(function () {
      const $tabs = $(this);
      const tabs = $tabs[0];

      // Foundation tabs doesnt have an additional wrapper by default, so add it if exists.
      var container;
      if ($tabs.parents('.moretabs:first').length) {
        container = $tabs.parents('.moretabs:first').get(0);
      } else {
        container = $(tabs).wrap('<div class="moretabs"></div>').parent().get(0);
      }

      const tabsItems = container.querySelectorAll('.tabs > li:not(.moretabs__more)');
      tabs.classList.add('moretabs__tabs');
      container.classList.add('moretabs--processed');
      // insert "more" button and duplicate the list
      tabs.insertAdjacentHTML(
        'beforeend',
        `
          <li class="moretabs__more">
            <button type="button" aria-haspopup="true" aria-expanded="false">
              ` +
          moretext +
          ` <i class="ico-db-more"></i>
            </button>
            <div class="moretabs__more-flyout">
              <ul class="menu vertical">
                ${tabs.innerHTML}
              </ul>
            </div>
          </li>
        `,
      );
      const secondary = container.querySelector('.moretabs__more-flyout');
      const secondaryItems = secondary.querySelectorAll('li');
      const allItems = container.querySelectorAll('li');
      const moreLi = tabs.querySelector('.moretabs__more');
      const moreBtn = moreLi.querySelector('button');
      moreBtn.addEventListener('click', (e) => {
        e.preventDefault();
        container.classList.toggle('moretabs--show-more');
        moreBtn.setAttribute('aria-expanded', container.classList.contains('moretabs--show-more'));
      });

      // adapt tabs
      const doAdapt = () => {
        // reveal all items for the calculation
        allItems.forEach((item) => {
          item.classList.remove('hidden');
        });

        // hide items that won't fit in the tabs
        let stopWidth = moreBtn.offsetWidth;
        let hiddenItems = [];
        const tabsWidth = tabs.offsetWidth;
        tabsItems.forEach((item, i) => {
          if (tabsWidth >= stopWidth + item.offsetWidth) {
            stopWidth += item.offsetWidth;
          } else {
            item.classList.add('hidden');
            hiddenItems.push(i);
          }
        });

        // toggle the visibility of More button and items in Secondary
        if (!hiddenItems.length) {
          moreLi.classList.add('hidden');
          container.classList.remove('moretabs--show-more');
          moreBtn.setAttribute('aria-expanded', false);
        } else {
          secondaryItems.forEach((item, i) => {
            if (!hiddenItems.includes(i)) {
              item.classList.add('hidden');
            }
          });
        }
      };
      // Register global events:
      // adapt on window resize:
      window.addEventListener('resize', doAdapt);

      // hide Secondary on the outside click:
      document.addEventListener('click', (e) => {
        let el = e.target;
        while (el) {
          if (el === secondary || el === moreBtn) {
            return;
          }
          el = el.parentNode;
        }
        container.classList.remove('moretabs--show-more');
        moreBtn.setAttribute('aria-expanded', false);
      });

      // RUN:
      doAdapt();
    });
  };
})(jQuery, Drupal);
