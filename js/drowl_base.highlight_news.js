/**
 * @file
 * Truncates text by given selector and adds a "... read more" link after the truncated textblock.
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_base_highlight_news = {
    attach: function (context, settings) {
      $('.block--highlight-news', context).each(function () {
        var $newsBlock = $(this);
        var newsTimestamp = $newsBlock.find('[data-news-cookie-timestamp]:first').data('news-cookie-timestamp');
        var cookieNewsTimestamp = Cookies.get('highlight_news_timestamp');
        // console.log('current news timestamp:');
        // console.log(newsTimestamp);
        // console.log('current users cookie timestamp:');
        // console.log(cookieNewsTimestamp);
        if (cookieNewsTimestamp === undefined || newsTimestamp > cookieNewsTimestamp) {
          $newsBlock.removeClass('hidden');
          $newsBlock.addClass('show');
        } else {
          $newsBlock.addClass('hidden');
          $newsBlock.removeClass('show');
        }
        $newsBlock.find('a, .close-button:first').on('click', function () {
          $newsBlock.addClass('hidden');
          Cookies.set('highlight_news_timestamp', newsTimestamp);
        });
      });
    },
  };
})(jQuery, Drupal);
