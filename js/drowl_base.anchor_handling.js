/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_base_anchorhandling = {
    attach: function (context, settings) {
      // Smooth anchor link scrolling (+ decrease scroll height by docked navbar height)
      var $smoothScrollTriggers = $(
        'a[href^="#"]:not([href="#"]):not([role="tab"]):not(.no-smooth):not([target="_blank"])',
      );
      $smoothScrollTriggers.each(function () {
        if ($($(this).attr('href')).length > 0) {
          // Only handle anchor links with existing target on the page, else do nothing! This prevents from breaking other JS # functionality
          new Foundation.SmoothScroll($(this), {
            offset: $('.top-bar-outer:first').outerHeight(),
          });
        }
      });

      // Smooth Scroll to anchor after loading
      if (window.location.hash && $(window.location.hash).length > 0) {
        $(window).on('load', function () {
          if ($('#' + window.location.hash.replace('#', '')).length > 0) {
            // Only handle anchor links with existing target on the page, else do nothing! This prevents from breaking other JS # functionality
            Foundation.SmoothScroll.scrollToLoc(window.location.hash, {
              threshold: 50,
              offset: $('.top-bar-outer:first').outerHeight(),
            });
          }
        });
      }
    },
  };

  // Create drowl_base sub-namespace:
  if (!Drupal.drowl_base.anchorhandling) {
    Drupal.drowl_base.anchorhandling = {};
  }
})(jQuery, Drupal);
