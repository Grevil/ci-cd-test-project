/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_base_language_switcher_dropdown = {
    attach: function (context, settings) {
      // Language switcher dropdown > Replace default label with current active / clicked language
      // TODO: Remove this if it works (it should: https://www.drupal.org/project/drupal/issues/2775651#comment-12637596)
      // setTimeout(function(){ // TODO: Quickfix for https://www.drupal.org/project/drupal/issues/2775651
      $('.language-switcher-dropdown', context).each(function () {
        var $wrapper = $(this);
        var $triggerLabel = $wrapper.find('.language-switcher-dropdown__current-label:first');
        var currentLanguage = $wrapper.find('.language-link.active-language:first').addClass('is-active').text();
        // Replace default string with current language
        if (currentLanguage.length > 0) {
          $triggerLabel.text(currentLanguage);
        }
        // Prevent FOUC
        $wrapper.removeClass('hidden');
        // Replace current string with clicked language string
        $wrapper.find('.language-link').on('click', function () {
          $triggerLabel.text($(this).text());
          // Also add active classes to the clicked lang - remove from others
          $wrapper.find('li, .language-link').removeClass('is-active').removeClass('active-language');
          $(this).addClass('is-active').addClass('active-language');
          $(this).parent('li').addClass('is-active');
        });
      });
      // }, 300);
    },
  };

  // Create drowl_base sub-namespace:
  if (!Drupal.drowl_base.language_switcher_dropdown) {
    Drupal.drowl_base.language_switcher_dropdown = {};
  }
})(jQuery, Drupal);
