(function ($, Drupal) {
  Drupal.behaviors.drowl_base_global = {
    attach: function (context, settings) {
      // Modify ZF accordion menus (before ZF init!)
      // $('ul.menu.accordion-menu ul', context).each(function(){
      //   // Duplicate first sub menu item for mobile so you can open the accordion as well as click on the menu item
      //   var parent = $(this).parent();
      //   $(this).prepend( "<li class='menu-item menu-item--overview-link'><a href='" + parent.find('a:first').attr('href') + "'><span class='overview-label'>" + Drupal.t('Overview') + "</span>" + parent.find('a:first').html() + "</a></li>" );

      //   // Add is-active class to current active trail items - what seems kinda impossible in the menu template
      //   if($(this).prev('a').hasClass('is-active')){
      //     $(this).addClass('is-active');
      //   }
      // });

      // ============================
      // Init zurb foundation scripts
      // ============================
      $(context).foundation();

      // TODO: Needs test! This problem may no longer exist with the new css variables solution for box-styles
      // Add box-style--nested class to nested box-style wrappers. We need this to ensure that the inner box-style dont get the colors from the outer one, if the CSS decleration of the outer one is defined later then the inner one (so it would override it)
      // $('.box-style .box-style', context).addClass('box-style--nested');

      // TODO: Solve with twig / php if possible (also we should check if ny block in this region is :visible?)
      // if ($('.top-bar-bottom-line').length) {
      //   $('body:first').addClass('top-bar-has-bottom-line');
      // }
    },
  };

  /**
   * TODO: Documentation!
   */
  Drupal.behaviors.drowl_base_global_ui_modifications = {
    attach: function (context, settings) {
      // Implement Javascript empty check - here is the WHY:
      // - https://www.drupal.org/node/953034 (not solved for years and probably never will be)
      // - There are workarounds for many, but not all, cases - this class is for unsolvable ones.
      // - Some workarounds will destroy Drupals caching, see: https://www.drupal.org/docs/8/api/render-api/cacheability-of-render-arrays
      $('.js-empty-check', context).each(function () {
        if ($(this).children().length) {
          $(this).removeClass('js-empty-check');
        } else {
          // Entirely remove the element so it doesnt affect CSS :first-child/:nth selectors
          $(this).replaceWith(
            '<!-- DROWL BASE Theme | js-empty-check removed element: ' + $(this).attr('class') + ' -->',
          );
        }
      });

      // Set Browser scrollbar width as CSS variable (basically to calculate the right width for our .viewport-width styles)
      document.documentElement.style.setProperty(
        '--global-scrollbar-width',
        Drupal.drowl_base.functions.getScrollbarWidth() + 'px',
      );

      // Sticky navbar optimizations
      $('#sticky-navbar', context)
        .on('sticky.zf.stuckto:top', function () {
          $(this).addClass('shrink');
          $('#top-bar-sticky-container').addClass('sticky-active');
        })
        .on('sticky.zf.unstuckfrom:top', function () {
          $(this).removeClass('shrink');
          $('#top-bar-sticky-container').removeClass('sticky-active');
        });

      $(document).ready(function () {
        // Check if the customer specific "media slideshow on viewport height"-option is set
        // and update the fallback height-CSS variable with the actual height determined here.
        document.querySelectorAll('.media-slideshow--height-viewport').forEach(function (slideshow) {
          slideshow.style.setProperty('--page-header-height', Drupal.drowl_base.functions.getPageHeaderHeight() + 'px');
        });
        // Make the actual .topbar height available as css variable (eg. usage inside the offcanvas menu, to have .offcanvas__top on the same height)
        document.documentElement.style.setProperty(
          '--global-topbar-height',
          document.querySelector('.top-bar-outer').offsetHeight + 'px',
        );
      });

      // Scroll to top
      $(document).on('scroll', function () {
        if ($(window).scrollTop() > 100) {
          $('.scroll-top', context).addClass('show');
        } else {
          $('.scroll-top', context).removeClass('show');
        }
      });
      $('.scroll-top', context).on('click', Drupal.drowl_base.functions.scrollToTop);

      // Wrap custom (wysiwyg) tables in foundations responsive table wrapper
      $('.field--type-text-long table, .paragraph--type-text table', context).each(function () {
        $(this).wrap('<div class="table-scroll"></div>');
      });

      // Mobile search button (topbar)
      // Open offcanvas layer and focus search field
      $('.mobile-search-button', context).on('click', function () {
        // TODO: without a timeout firefox (mobile) unfocuses the search field
        //       immidiatly after its focused. Maybe its because the
        //       search field is hidden till the offcanvas opening animation
        //       has finished. So we need to set focus in the event beyond
        //       opened.zf.offcanvas
        window.setTimeout(function () {
          $('.off-canvas__top .search-form .form-search')[0].focus();
        }, 1000);
      });

      // Set body class if the offcanvas menu is opened
      $(document).on('opened.zf.offcanvas', function () {
        $('body', context).addClass('offcanvas-menu-opened');
      });
      $(document).on('closed.zf.offcanvas', function () {
        $('body', context).removeClass('offcanvas-menu-opened');
      });

      // Pause Slideshow (slick) autoplay if a video slide is currently playing
      $('.slick [data-video-provider]', context).each(function () {
        $(this).on('playstart', function () {
          $(this).parents('.slick-slider:first').slick('slickPause');
        });
        $(this).on('paused', function () {
          $(this).parents('.slick-slider:first').slick('slickPlay');
        });
      });
      // Initialize by class / by adding the needed additonal wrapper
      $('.form-select--fake-placeholder', context).selectFakePlaceholder();

      // Privacy form checkboxes => make the field description clickable (because there is no label)
      $('.form-item-privacypolicy-accept', context).each(function () {
        $(this)
          .find('.description:first')
          .on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).parents('.form-item-privacypolicy-accept:first').find('label').trigger('click');
          });
      });
    },
  };

  /**
   * Set the scrollbar width of the current browser
   * For the width calculation in our viewport-width
   * mixin / formatter class.
   * Original source: https://stackoverflow.com/a/13382873/6266306
   */
  // TODO: This function should live in drowl_base.functions.js!
  Drupal.drowl_base.functions.getScrollbarWidth = function () {
    // Creating invisible container
    const outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.overflow = 'scroll'; // forcing scrollbar to appear
    document.body.appendChild(outer);

    // Creating inner element and placing it in the container
    const inner = document.createElement('div');
    outer.appendChild(inner);

    // Calculating difference between container's full width and the child width
    const scrollbarWidth = outer.offsetWidth - inner.offsetWidth;

    // Removing temporary elements from the DOM
    outer.parentNode.removeChild(outer);

    return scrollbarWidth;
  };
})(jQuery, Drupal);
