/**
 * @file
 * Truncates text by given selector and adds a "... read more" link after the truncated textblock.
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_base_read_more = {
    attach: function (context, settings) {
      // Initialize by class
      $('.read-more', context).readMore();
    },
  };

  // Create drowl_base sub-namespace:
  if (!Drupal.drowl_base.read_more) {
    Drupal.drowl_base.read_more = {};
  }

  /**
   * TODO: Documentation!
   */
  $.fn.readMore = function (charamount, ellipsestext, moretext, lesstext, respectfullworlds) {
    // Initialize default variables
    charamount = charamount === undefined ? 180 : charamount;
    ellipsestext = ellipsestext === undefined ? Drupal.t('...') : ellipsestext;
    moretext = moretext === undefined ? Drupal.t('Read more') : moretext;
    lesstext = lesstext === undefined ? Drupal.t('Collapse') : lesstext;
    respectfullworlds = respectfullworlds === undefined ? true : respectfullworlds;

    this.each(function () {
      var textwrapper = $(this);
      var content = $(this).html();
      var teaserText;
      var restText;
      var html;
      // Remove comments from HTML otherwise we might see weird results with WDEBuG:
      content = content.replace(/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->)(.|\n))*-->/g, '');

      // TODO add data attributes as override for defaults (if data attributes existing)

      if (content.length > charamount) {
        textwrapper.addClass('read-more--active').addClass('read-more--less');
        if (respectfullworlds) {
          teaserText = content.substr(0, content.lastIndexOf(' ', charamount));
          restText = content.substr(content.lastIndexOf(' ', charamount), content.length - charamount);
        } else {
          teaserText = content.substr(0, charamount);
          restText = content.substr(charamount, content.length - charamount);
        }
        // Repair potentially broken HTML in substrings:
        teaserText = new DOMParser().parseFromString(teaserText, 'text/html').body.innerHTML;
        restText = new DOMParser().parseFromString(restText, 'text/html').body.innerHTML;
        html =
          teaserText +
          '<span class="read-more__moreellipses">' +
          ellipsestext +
          '</span> <span class="read-more__morecontent">' +
          restText +
          '</span>' +
          '&nbsp;<a href="" class="read-more__morelink">' +
          moretext +
          '</a>';
        $(this).html(html);
      }

      $(this)
        .find('.read-more__morelink:first')
        .click(function () {
          if (textwrapper.hasClass('read-more--less')) {
            textwrapper.removeClass('read-more--less');
            $(this).html(lesstext);
          } else {
            textwrapper.addClass('read-more--less');
            $(this).html(moretext);
          }
          return false;
        });
    });
  };
})(jQuery, Drupal);
