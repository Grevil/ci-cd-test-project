(function ($, Drupal) {
  Drupal.behaviors.drowl_base_webform_placeholder_labels = {
    attach: function (context, settings) {
      // Show hidden labels if fields are filled (so the placeholder is gone and the field is unlabeled)
      // To us this the field label need to set to "invisible" (& a placeholder should set).
      if ($('.webform-submission-form').length) {
        $('.webform-submission-form .form-item.form-no-label', context).each(function () {
          let $formItem = $(this);
          let $label = $(this).children('label:first');
          let labelText = $label.attr('aria-label');
          let $input = $(this).find('input:not([type=checkbox]):not([type=radio]):first, textarea:first, select:first');
          $formItem.addClass('form-item--show-label-on-edit');
          $input.on('change', function () {
            $label.text(labelText);
            if ($(this).val().trim() != '') {
              $formItem.addClass('form-item--filled');
            } else {
              $formItem.removeClass('form-item--filled');
            }
          });
        });
      }
    },
  };
})(jQuery, Drupal);
